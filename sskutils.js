import { Server } from 'ws';
import * as WebSocket$1 from 'ws';

const Uid = function (prefix = '') {
    let i = 0;
    const get = function () {
        const v = prefix + i.toString();
        i += 1;
        return v;
    };
    return get;
};
const type = (function () {
    const isDefined = function (v) {
        return typeof v !== 'undefined' && v !== null;
    };
    const isUndefined = function (v) {
        return !isDefined(v);
    };
    const isString = function (v) {
        return isDefined(v) && typeof v === 'string';
    };
    const isObject = function (v) {
        return isDefined(v) && typeof v === 'object' && !isArray(v);
    };
    const isArray = function (v) {
        return Array.isArray(v);
    };
    const isFunction = function (v) {
        return isDefined(v) && typeof v === 'function';
    };
    const isFunctionable = function (f, ...args) {
        if (!isFunction(f)) {
            return;
        }
        return f.apply({}, args);
    };
    const isBoolean = function (v) {
        return isDefined(v) && typeof v === 'boolean';
    };
    const isBool = isBoolean;
    const isNumber = function (v) {
        return isDefined(v) && typeof v === 'number';
    };
    const forIn = function (o, cb) {
        if (!isObject(o)) {
            return;
        }
        const keys = Object.keys(o);
        keys.forEach(cb);
    };
    const inArray = function (v, arr) {
        return (arr.indexOf(v) >= 0);
    };
    return {
        forIn,
        inArray,
        isArray,
        isBool,
        isBoolean,
        isDefined,
        isFunction,
        isFunctionable,
        isNumber,
        isObject,
        isString,
        isUndefined,
    };
}());
const promiseCache = function () {
    const promises = {};
    const get = function (id) {
        if (type.isDefined(promises[id])) {
            return promises[id].promise;
        }
        let resolve;
        let reject;
        const cb = (resolveArg, rejectArg) => {
            resolve = resolveArg;
            reject = rejectArg;
        };
        const promise = new Promise(cb);
        promises[id] = {
            promise,
            reject,
            resolve,
        };
        return promises[id].promise;
    };
    const deregister = function (id) {
        if (!type.isDefined(promises[id])) {
            return;
        }
        delete promises[id];
    };
    const resolve = function (id, v) {
        if (!type.isDefined(promises[id])) {
            return;
        }
        promises[id].resolve(v);
        deregister(id);
    };
    const reject = function (id, v) {
        if (!type.isDefined(promises[id])) {
            return;
        }
        promises[id].reject(v);
        deregister(id);
    };
    return {
        get: get,
        reject: reject,
        resolve: resolve
    };
};
class PropWrap {
    constructor(props) {
        if (!type.isObject(props)) {
            throw new Error('Propwrap is expecting an object babe');
        }
        this._props = props;
    }
    hasProp(k) {
        return type.isDefined(this._props[k]);
    }
    propIs(k, v) {
        return this.hasProp(k) && this.getProp(k) === v;
    }
    getProp(k) {
        if (!this.hasProp(k)) {
            throw new Error('Prop not defined');
        }
        return this._props[k];
    }
    setProp(k, v) {
        this._props[k] = v;
    }
    serialize() {
        return JSON.parse(JSON.stringify(this._props));
    }
}
const randomizer = (function () {
    const alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
    const numbers = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];
    const chars = alphabet.concat(numbers, numbers);
    const getRandomAlphaNum = function () {
        return alphabet[Math.floor(Math.random() * alphabet.length)];
    };
    const getRandomAlpha = function () {
        return chars[Math.floor(Math.random() * chars.length)];
    };
    const getRandomNumber = function () {
        return (numbers[Math.floor(Math.random() * numbers.length)]).toString();
    };
    return {
        getRandomAlphaNum,
        getRandomAlpha,
        getRandomNumber,
    };
}());
const generateToken = function (optArg = {}) {
    const typesFactory = {
        alphanumeric: 'getRandomAlphaNum',
        alpha: 'getRandomAlpha',
        numeric: 'getRandomNumber',
    };
    const options = optArg;
    options['length'] = (!type.isNumber(options['length'])) ? 4 : options['length'];
    options['startsWithAlpha'] = (!type.isBool(options['startsWithAlpha'])) ? true : options['startsWithAlpha'];
    options['type'] = (!type.isDefined(typesFactory[options['type']])) ? 'alphanumeric' : options['type'];
    let typeMethod = randomizer[typesFactory[options['type']]];
    let length = options['length'];
    const r = [];
    if (options['startsWithAlpha']) {
        r.push(typeMethod());
        length -= 1;
    }
    for (let i = 0; i < length; i++) {
        r.push(typeMethod());
    }
    return r.join('');
};
const generateKeyCode = function (optArg = {}) {
    const options = optArg;
    options['length'] = (!type.isNumber(options['length'])) ? 4 : options['length'];
    const r = [];
    for (let i = 0; i < options['length']; i++) {
        r.push(randomizer.getRandomNumber());
    }
    return r.join('');
};


var helpers = Object.freeze({
	Uid: Uid,
	type: type,
	promiseCache: promiseCache,
	PropWrap: PropWrap,
	randomizer: randomizer,
	generateToken: generateToken,
	generateKeyCode: generateKeyCode
});

var constants;
(function (constants) {
    constants["askHandle"] = "ask";
    constants["deregisterEventListenerHandle"] = "deregisterEventListenerHandle";
    constants["registerEventListenerHandle"] = "registerEventListenerHandle";
    constants["registrationHandle"] = "register";
    constants["triggerEventHandler"] = "triggerEventHandler";
    constants["requestRoomAccessKey"] = "requestRoomAccessKey";
    constants["requestRoomAccessByKey"] = "requestRoomAccessByKey";
})(constants || (constants = {}));

const eventUid = Uid('X0__EVT__');
class EventMngr {
    constructor(eventTypes) {
        if (!type.isArray(eventTypes) && !type.isUndefined(eventTypes)) {
            throw new Error('The eventtypes must be undefined or an array');
        }
        this._eventTypes = eventTypes;
        this._listeners = {};
    }
    addEventType(eventType) {
        if (!type.isString(eventType)) {
            throw new Error('EventType should be a string');
        }
        if (!type.isArray(this._eventTypes)) {
            this._eventTypes = [];
        }
        this._eventTypes.push(eventType);
    }
    allowedEvent(event) {
        return type.isUndefined(this._eventTypes) || type.inArray(event, this._eventTypes);
    }
    on(event, cb) {
        if (!this.allowedEvent(event)) {
            throw new Error(`Provided event not supported; available events: ${(this._eventTypes.join(','))}`);
        }
        if (type.isUndefined(this._listeners[event])) {
            this._listeners[event] = {};
        }
        const eventID = eventUid();
        this._listeners[event][eventID] = cb;
        const deregister = () => {
            delete this._listeners[event][eventID];
        };
        const isEventListenerReference = () => type.isDefined(this._listeners[event]) && type.isDefined(this._listeners[event][eventID]);
        return {
            deregister,
            eventID,
            isEventListenerReference,
        };
    }
    once(event, cb) {
        let deregister;
        const wrappedCb = (evtData) => {
            cb(evtData);
            deregister();
        };
        const r = this.on(event, wrappedCb);
        deregister = r.deregister;
        return r;
    }
    trigger(event, eventData) {
        if (!this.allowedEvent(event)) {
            throw new Error(`Provided event not supported; available events: ${this._eventTypes.join(',')}`);
        }
        if (type.isUndefined(this._listeners[event])) {
            return;
        }
        const _eventListeners = this._listeners[event];
        let canceled = false;
        const cancelFn = function () {
            canceled = true;
        };
        type.forIn(_eventListeners, (eventID) => {
            if (!canceled) {
                _eventListeners[eventID](eventData, cancelFn);
            }
        });
    }
    exposee(obj) {
        obj.on = (event, cb) => {
            this.on(event, cb);
        };
        obj.once = (event, cb) => {
            this.once(event, cb);
        };
    }
    getEventTypes() {
        return this._eventTypes;
    }
}

const c = {
    roomIdKey: 'roomId',
    roomEvents: {
        participantJoin: 'participantJoin',
    }
};
class WsRoom extends PropWrap {
    constructor(proporties = {}) {
        super(proporties);
        this._participants = {};
        this._eventUid = {};
        this._events = {};
        this._roomEvents = new EventMngr(Object.keys(c.roomEvents));
        this.roomEvents = {};
        this._roomEvents.exposee(this.roomEvents);
    }
    set roomId(v) {
        if (this.hasProp(c.roomIdKey)) {
            throw new Error('Room id can not be overwritten');
        }
        this._participantUid = Uid(`${v}X0PART`);
        this._eventUid = Uid(`${v}X0EVT`);
        this.setProp(c.roomIdKey, v);
    }
    get roomId() {
        if (!this.hasProp(c.roomIdKey)) {
            return;
        }
        return this.getProp(c.roomIdKey);
    }
    isRoom() {
        return true;
    }
    acceptParticipant(identifierInst, wsRoomSocketServerInst) {
        return this.addParticipant(identifierInst, wsRoomSocketServerInst);
    }
    findParticipant(lookupObj, excludeParticipantId = false) {
        const r = [];
        type.forIn(this._participants, (pId) => {
            const participant = this._participants[pId];
            if (pId === excludeParticipantId) {
                return;
            }
            let acceptParticipant = true;
            type.forIn(lookupObj, (attr) => {
                if (!participant.identifierInst.propIs(attr, lookupObj[attr])) {
                    acceptParticipant = false;
                }
            });
            if (acceptParticipant) {
                r.push(participant);
            }
        });
        return r;
    }
    ask(participantId, lookupObj, method, ...args) {
        const participants = this.findParticipant(lookupObj, participantId);
        if (participants.length < 1) {
            const msg = 'Couldnt match any participant';
            throw new Error(msg);
        }
        if (participants.length > 1) {
            return new Promise((resolve, reject) => {
                participants.forEach((participant) => {
                    participant.wsRoomSocketServerInst.call(...([method].concat(args)));
                });
                resolve('todo should respond with all results');
            });
        }
        const participant = participants[0];
        return participant.wsRoomSocketServerInst.call(...([method].concat(args))).catch((e) => {
            throw new Error(`Participant replied with error: ${e}`);
        });
    }
    registerEventListener(participantId, event) {
        const eventID = this._eventUid();
        this._events[eventID] = {
            event,
            participantId,
        };
        return eventID;
    }
    deregisterEventListener(pId, eventID) {
        if (!type.isDefined(this._events[eventID])) {
            return true;
        }
        if (this._events[eventID].participantId !== pId) {
            throw new Error("this event isn't owned by this socket so you can not disable it");
        }
        delete this._events[eventID];
        return true;
    }
    triggerEvent(participantId, event, eventData) {
        const participantsEvents = {};
        let hasEvents = false;
        type.forIn(this._events, (eventID) => {
            const eventInst = this._events[eventID];
            if (eventInst.event !== event || eventInst.participantId === participantId) {
                return;
            }
            if (!type.isArray(participantsEvents[eventInst.participantId])) {
                participantsEvents[eventInst.participantId] = [];
            }
            hasEvents = true;
            participantsEvents[eventInst.participantId].push(eventID);
        });
        if (!hasEvents) {
            return;
        }
        type.forIn(participantsEvents, (partId) => {
            this._participants[partId].wsRoomSocketServerInst.call(constants.triggerEventHandler, participantsEvents[partId], eventData);
        });
        return;
    }
    removeEventsForParticipant(participantID) {
        type.forIn(this._events, (eventID) => {
            if (this._events[eventID].participantId === participantID) {
                delete this._events[eventID];
            }
        });
    }
    removeParticipant(participantID) {
        if (!type.isObject(this._participants[participantID])) {
            return;
        }
        this.removeEventsForParticipant(participantID);
        this._participants[participantID].wsRoomSocketServerInst.clearAllHandles();
        this._participants[participantID].wsRoomSocketServerInst.close();
        delete this._participants[participantID];
    }
    addParticipant(identifierInst, wsRoomSocketServerInst) {
        if (!this.hasProp(c.roomIdKey)) {
            throw new Error('Room not yet properly initiated');
        }
        const pId = this._participantUid();
        wsRoomSocketServerInst.clearAllHandles();
        wsRoomSocketServerInst.addHandle(constants.askHandle, (lookupObj, method, ...args) => {
            return this.ask(pId, lookupObj, method, ...args);
        });
        wsRoomSocketServerInst.addHandle(constants.registerEventListenerHandle, (event) => this.registerEventListener(pId, event));
        wsRoomSocketServerInst.addHandle(constants.deregisterEventListenerHandle, (eventID) => this.deregisterEventListener(pId, eventID));
        wsRoomSocketServerInst.addHandle(constants.triggerEventHandler, (event, eventData) => this.triggerEvent(pId, event, eventData));
        wsRoomSocketServerInst.once('close', () => {
            this.triggerEvent(pId, 'participant.disconnected', identifierInst.serialize());
            return this.removeParticipant(pId);
        });
        this._participants[pId] = {
            participantId: pId,
            identifierInst,
            wsRoomSocketServerInst,
        };
        const isMatchObject = function () {
            return true;
        };
        this._roomEvents.trigger(c.roomEvents.participantJoin, this._participants[pId]);
        this.triggerEvent(pId, 'participant.connected', identifierInst.serialize());
        return {
            isMatchObject,
            participantId: pId,
            roomId: this.roomId,
        };
    }
}

var messageTypes;
(function (messageTypes) {
    messageTypes["default"] = "default";
    messageTypes["error"] = "error";
    messageTypes["response"] = "response";
    messageTypes["rpc"] = "rpc";
})(messageTypes || (messageTypes = {}));

class Factory {
    constructor() {
        this.factories = {};
    }
    hasFactoryMethod(key) {
        return type.isFunction(this.factories[key]);
    }
    addFactoryMethod(key, method) {
        if (this.hasFactoryMethod(key)) {
            throw new Error(` Factory method with key ${key} already defined. `);
        }
        if (!type.isFunction(method)) {
            throw new Error(` Factory method ${method} is not a function. `);
        }
        this.factories[key] = method;
        return {
            delete: () => {
                return this.deleteFactoryMethod(key);
            },
            getKey: () => {
                return key;
            }
        };
    }
    deleteFactoryMethod(key) {
        if (!this.hasFactoryMethod(key)) {
            throw new Error(` Factory method with key ${key} does not exists. `);
        }
        delete this.factories[key];
        return true;
    }
    getFactoryMethod(key) {
        if (!this.hasFactoryMethod(key)) {
            throw new Error(` Factory method with key ${key} does not exists. `);
        }
        return this.factories[key];
    }
    clear() {
        this.factories = {};
    }
    handle(key, context, argsArg) {
        const method = this.getFactoryMethod(key);
        let args = argsArg;
        if (!type.isArray(argsArg)) {
            args = [argsArg];
        }
        const r = method.apply(context, args);
        if (!type.isDefined(r)) {
            return new Promise((resolve) => { resolve(); });
        }
        return new Promise((resolve, reject) => {
            try {
                resolve(r);
            }
            catch (e) {
                reject(e);
            }
        });
    }
}

const constants$1 = {
    idKey: 'messageID',
};
class Message {
    constructor(arg) {
        this.headers = {};
        this.content = {};
        this.type = messageTypes.default;
        if (type.isString(arg)) {
            this.deserialize(arg);
        }
    }
    isMessage() {
        return true;
    }
    addHeader(name, value) {
        this.headers[name] = value;
        return this;
    }
    getHeader(name) {
        return this.headers[name];
    }
    addContent(name, value) {
        this.content[name] = value;
        return this;
    }
    hasContent(name) {
        return type.isDefined(this.content[name]);
    }
    getContent(name) {
        if (!this.hasContent(name)) {
            throw new Error(`Content ${name} ain't defined`);
        }
        return this.content[name];
    }
    setMessageType(v) {
        this.type = v;
    }
    getMessageType() {
        return this.type;
    }
    validate() {
        return type.isDefined(this.type) && type.isDefined(this.id);
    }
    set id(v) {
        this.headers[constants$1.idKey] = v;
    }
    get id() {
        return this.headers[constants$1.idKey];
    }
    serialize() {
        if (!this.validate()) {
            throw new Error('invalid message, unable to serialize.');
        }
        const o = {
            content: this.content,
            headers: this.headers,
            type: this.type,
        };
        return JSON.stringify(o);
    }
    deserialize(serializedContent) {
        if (!type.isString(serializedContent)) {
            throw new Error('Desirialisation can only be done from a string');
        }
        const o = JSON.parse(serializedContent);
        if (!type.isDefined(o.type)) {
            throw new Error('Not a proper message object, missing a type');
        }
        this.setMessageType(o.type);
        if (type.isObject(o.headers)) {
            type.forIn(o.headers, (v) => {
                this.addHeader(v, o.headers[v]);
            });
        }
        if (type.isObject(o.content)) {
            type.forIn(o.content, (v) => {
                this.addContent(v, o.content[v]);
            });
        }
        return true;
    }
    generateReply(...arg) {
        return false;
    }
}

const getMessageType = function (arg) {
    if (!type.isString(arg)) {
        return;
    }
    try {
        const o = JSON.parse(arg);
        return o.type;
    }
    catch (e) {
        return;
    }
};

const constants$2 = {
    answerName: 'answerName',
    argumentsName: 'argumentsName',
    errorName: 'errorName',
    initiatorIdName: 'initiatorIdName',
    procedureName: 'procedureName',
};
class RpcMessage extends Message {
    constructor(arg) {
        super(arg);
        this.type = messageTypes.rpc;
    }
    expectResponse() {
        return true;
    }
    validate() {
        if (!super.hasContent(constants$2.procedureName)) {
            return false;
        }
        return super.validate();
    }
    set procedure(procedureName) {
        super.addContent(constants$2.procedureName, procedureName);
    }
    get procedure() {
        return super.getContent(constants$2.procedureName);
    }
    set arguments(args) {
        super.addContent(constants$2.argumentsName, args);
    }
    get arguments() {
        return super.getContent(constants$2.argumentsName);
    }
    generateReply(messageId, answer) {
        const responseMessageInst = new ResponseMessage(super.serialize());
        responseMessageInst.initiatorId = this.id;
        responseMessageInst.id = messageId;
        responseMessageInst.answer = answer;
        return responseMessageInst;
    }
    generateError(messageId, error) {
        const errorMessageInst = new ErrorMessage(super.serialize());
        errorMessageInst.initiatorId = this.id;
        errorMessageInst.id = messageId;
        errorMessageInst.error = error;
        return errorMessageInst;
    }
    addContent(name, value) {
        if (name === constants$2.procedureName) {
            this.procedure = value;
            return value;
        }
        else if (name === constants$2.argumentsName) {
            this.arguments = value;
            return value;
        }
        return super.addContent(name, value);
    }
}
class ResponseMessage extends RpcMessage {
    constructor(arg) {
        super(arg);
        this.type = messageTypes.response;
    }
    expectResponse() {
        return true;
    }
    validate() {
        if (!super.hasContent(constants$2.initiatorIdName) || !super.hasContent(constants$2.answerName)) {
            return false;
        }
        return super.validate();
    }
    set initiatorId(v) {
        super.addContent(constants$2.initiatorIdName, v);
    }
    get initiatorId() {
        return super.getContent(constants$2.initiatorIdName);
    }
    set answer(v) {
        super.addContent(constants$2.answerName, v);
    }
    get answer() {
        return super.getContent(constants$2.answerName);
    }
    addContent(name, value) {
        if (name === constants$2.initiatorIdName) {
            this.initiatorId = value;
            return value;
        }
        return super.addContent(name, value);
    }
    generateReply() {
        return false;
    }
    generateError() {
        return false;
    }
}
class ErrorMessage extends RpcMessage {
    constructor(arg) {
        super(arg);
        this.type = messageTypes.error;
    }
    expectResponse() {
        return true;
    }
    validate() {
        if (!this.hasContent(constants$2.initiatorIdName) || !this.hasContent(constants$2.errorName)) {
            return false;
        }
        return super.validate();
    }
    set initiatorId(v) {
        super.addContent(constants$2.initiatorIdName, v);
    }
    get initiatorId() {
        return super.getContent(constants$2.initiatorIdName);
    }
    set error(v) {
        super.addContent(constants$2.errorName, v);
    }
    get error() {
        return super.getContent(constants$2.errorName);
    }
    addContent(name, value) {
        if (name === constants$2.initiatorIdName) {
            this.initiatorId = value;
            return value;
        }
        return super.addContent(name, value);
    }
    generateReply() {
        return false;
    }
    generateError() {
        return false;
    }
}

const t$1 = type;
const eventUid$1 = Uid('X0SCKABST__EVT__');
const messageUid = Uid('X0SCKABST__MSG__');
const socketEvents = {
    message: 'message',
    close: 'close',
    error: 'error',
    open: 'open',
};
const messageTypeClasses = {};
messageTypeClasses[messageTypes.rpc] = RpcMessage;
messageTypeClasses[messageTypes.response] = ResponseMessage;
messageTypeClasses[messageTypes.error] = ErrorMessage;
class SocketAbstract {
    constructor() {
        this._eventListeners = {};
        this._messageFactory = new Factory();
        this._rpcPromiseHandler = promiseCache();
        this.on(socketEvents.message, (msgString, cancelFn, replyFn, catchFn) => {
            try {
                const result = this._runMsgFactory(msgString);
                if (result instanceof Promise) {
                    result.then((r) => {
                        replyFn(r);
                    });
                    result.catch((e) => {
                        catchFn(e);
                    });
                }
            }
            catch (e) {
                catchFn(e);
            }
        });
    }
    addHandle(name, method) {
        if (!t$1.isString(name)) {
            throw new Error('Adding a handle requires a handle name');
        }
        if (this._messageFactory.hasFactoryMethod(name)) {
            throw new Error(`The handle ${name} has already been declared or is reserved `);
        }
        return this._messageFactory.addFactoryMethod(name, method);
    }
    removeHandle(name) {
        if (this._messageFactory.hasFactoryMethod(name)) {
            return this._messageFactory.deleteFactoryMethod(name);
        }
        return true;
    }
    clearAllHandles() {
        this._messageFactory.clear();
    }
    _runMsgFactory(msgString) {
        const msgType = getMessageType(msgString);
        if (!t$1.isDefined(msgType)) {
            return;
        }
        const msgInstance = new messageTypeClasses[msgType](msgString);
        if (msgType === messageTypes.rpc) {
            return this._messageFactory.handle(msgInstance.procedure, this, msgInstance.arguments);
        }
        else if (msgType === messageTypes.response) {
            this._rpcPromiseHandler.resolve(msgInstance.initiatorId, msgInstance.answer);
        }
        else if (msgType === messageTypes.error) {
            this._rpcPromiseHandler.reject(msgInstance.initiatorId, msgInstance.error);
        }
    }
    on(event, cb) {
        if (!t$1.isDefined(socketEvents[event])) {
            throw new Error(`Event ${event} unavailable`);
        }
        if (!t$1.isDefined(this._eventListeners[event])) {
            this._eventListeners[event] = {};
        }
        const eventId = eventUid$1();
        this._eventListeners[event][eventId] = cb;
        const deregister = () => {
            delete this._eventListeners[event][eventId];
        };
        return {
            eventId,
            deregister
        };
    }
    once(event, cb) {
        let deregister;
        const wrappedCb = (evtData) => {
            cb(evtData);
            deregister();
        };
        const r = this.on(event, wrappedCb);
        deregister = r.deregister;
        return r;
    }
    _trigger(event, eventData) {
        if (!t$1.isDefined(socketEvents[event])) {
            throw new Error(`Event ${event} unavailable`);
        }
        const eventListeners = this._eventListeners[event];
        let canceled = false;
        const cancelFn = function () {
            canceled = true;
        };
        let replyFn = (...arg) => { };
        let catchFn = (...arg) => { };
        if (event === socketEvents.message) {
            replyFn = (answer) => {
                const msgInst = new messageTypeClasses[getMessageType(eventData)](eventData);
                if (t$1.isDefined(answer)) {
                    const answerInst = msgInst.generateReply(messageUid(), answer);
                    if (answerInst !== false) {
                        this.send(answerInst);
                    }
                }
            };
            catchFn = (error) => {
                const msgInst = new messageTypeClasses[getMessageType(eventData)](eventData);
                const errorInst = msgInst.generateError(messageUid(), error.message);
                if (errorInst !== false) {
                    this.send(errorInst);
                }
            };
        }
        t$1.forIn(eventListeners, (listener) => {
            if (!canceled) {
                eventListeners[listener](eventData, cancelFn, replyFn, catchFn);
            }
        });
    }
    getMessage(messageType) {
        if (!t$1.isDefined(messageTypeClasses[messageType])) {
            throw new Error(`The messagetype ${messageType} does not exists what are you thinking!`);
        }
        const msgInstance = new messageTypeClasses[messageType]();
        msgInstance.id = messageUid();
        return msgInstance;
    }
    _send(...arg) {
        throw new Error('And this is something that the implementor should have overwritten!!!!');
    }
    close(...arg) {
        throw new Error('And this is something that the implementor should have overwritten!!!!');
    }
    send(msgInst) {
        if (!(msgInst instanceof Message)) {
            throw new Error('Provide instance of Message to send, you a-hole');
        }
        this._send(msgInst.serialize());
    }
    call(procedure, ...args) {
        const rpcMsg = this.getMessage('rpc');
        rpcMsg.procedure = procedure;
        rpcMsg.arguments = args;
        const p = this._rpcPromiseHandler.get(rpcMsg.id);
        this.send(rpcMsg);
        return p;
    }
    getEventTypes() {
        return Object.keys(socketEvents);
    }
    allowedEvent(event) {
        return t$1.isDefined(socketEvents[event]);
    }
}

class SocketServer extends SocketAbstract {
    constructor(wsSocket) {
        super();
        this._wsSocket = wsSocket;
        this._wsSocket.on('message', (evt) => {
            super._trigger('message', evt);
        });
        this._wsSocket.on('close', (evt) => {
            super._trigger('close', evt);
        });
    }
    _send(msgString) {
        this._wsSocket.send(msgString);
    }
    close() {
        this._wsSocket.close();
    }
}

class WsRoomSocketServer extends SocketServer {
    isWsRoomSocketServer() {
        return true;
    }
}

const WebSocketServer = Server;
const t = type;
const roomUid = Uid('X0ROOM');
const events = {
    onConnect: 'onConnect',
    onRegistration: 'onRegistration',
    onRoomRegistration: 'onRoomRegistration',
    onRoomRemoval: 'onRoomRemoval',
};
class WsRooms {
    constructor(httpServer) {
        this._wsServer = new WebSocketServer({ server: httpServer });
        this._events = new EventMngr(Object.keys(events));
        this._events.exposee(this);
        this._rooms = {};
        this._props = {};
        this._wsServer.on('connection', (socketArg) => {
            this._onConnect(socketArg);
        });
        this.authMethod = (authObj, acceptCb) => {
            acceptCb();
        };
    }
    isRooms() {
        return true;
    }
    _onConnect(socketArg) {
        const socketInst = new WsRoomSocketServer(socketArg);
        this._events.trigger(events.onConnect, socketInst);
        socketInst.addHandle(constants.registrationHandle, (authObj) => {
            let isAccepted = false;
            let identifierInst = false;
            const accept = () => (isAccepted = true);
            const reject = () => (isAccepted = false);
            return Promise.resolve(this.authMethod(authObj, accept, reject)).then((identifierInstArg) => {
                identifierInst = (t.isFunctionable(identifierInstArg.isIdentifier) === true) ? identifierInstArg : false;
                if (isAccepted !== true) {
                    throw new Error('Unable to register the connection');
                }
                return { accepted: true, msg: 'You\'re in, in like Flynn' };
            }).then((rArg) => {
                let r = rArg;
                if (rArg.accepted === true) {
                    const match = this._matchRoom(identifierInst, socketInst);
                    if (t.isObject(match) && type.isFunctionable(match['isMatchObject']) === true) {
                        r = Object.assign(r, match);
                        delete r['isMatchObject'];
                    }
                }
                return r;
            }).catch(() => {
                console.log('nope mate that plane won\'t go up');
            });
        });
    }
    _matchRoom(identifierInst, socketInst) {
        let match = false;
        type.forIn(this._rooms, (roomId) => {
            const matchObject = this._rooms[roomId].acceptParticipant(identifierInst, socketInst);
            if (!match && type.isObject(matchObject) && type.isFunctionable(matchObject.isMatchObject) === true) {
                match = matchObject;
            }
        });
        return match;
    }
    set authMethod(v) {
        if (!t.isFunction(v)) {
            throw new Error('The authentication method must be a function');
        }
        this._props.authMethod = v;
    }
    get authMethod() {
        return this._props.authMethod;
    }
    addRoom(roomInst) {
        const roomID = roomUid();
        roomInst.roomId = roomID;
        if (!t.isFunction(roomInst.isRoom) || roomInst.isRoom() !== true) {
            throw new Error('Provided argument should be an instance of the type Room.');
        }
        this._rooms[roomID] = roomInst;
        const deregister = () => {
            this._events.trigger(events.onRoomRemoval, roomID);
            delete this._rooms[roomID];
        };
        const isRoomReference = () => type.isDefined(this._rooms[roomID]);
        const getRoom = () => this._rooms[roomID];
        const result = {
            deregister,
            getRoom,
            isRoomRef: isRoomReference,
            isRoomReference,
            roomID,
        };
        this._events.trigger(events.onRoomRegistration, result);
        return result;
    }
}

const t$2 = type;
class SocketClient extends SocketAbstract {
    constructor(host) {
        super();
        this._conf = {
            keepAlive: false
        };
        if (!t$2.isString(host)) {
            throw new Error('Please provide a proper host');
        }
        this.host = host;
        this.running = false;
        this.setup();
    }
    set keepAlive(v) {
        if (!t$2.isBool(v)) {
            throw new Error('KeepAlive should be numeric value');
        }
        this._conf.keepAlive = v;
    }
    get keepAlive() {
        return this._conf.keepAlive;
    }
    setup() {
        this._socket = new WebSocket(this.host);
        this.running = true;
        this._socket.onopen = (evt) => {
            this._trigger('open', evt);
        };
        this._socket.onmessage = (evt) => {
            this._trigger('message', evt.data);
        };
        this._socket.onclose = () => {
            this.onClose();
            this._trigger('close', {});
        };
        this._socket.onerror = (evt) => {
            this._trigger('error', evt);
        };
    }
    onClose() {
        this.running = false;
        if (this._conf.keepAlive) {
            this.setup();
        }
    }
    isRunning() {
        return this.running;
    }
    _send(msgString) {
        this._socket.send(msgString);
    }
}

const localEvents = {
    activation: 'activation',
};
class WsRoomSocketClient {
    constructor(host, authObject) {
        this._evtInst = new EventMngr(Object.keys(localEvents));
        this._authObject = authObject;
        this._registrationResult = {};
        this._active = false;
        this._remoteEvents = {};
        this._socketClient = new SocketClient(host);
        this._socketClient.once('open', () => {
            this._register();
            this._setupTriggerEventHandlers();
        });
    }
    _register() {
        this._socketClient.call(constants.registrationHandle, this._authObject).then((registrationResult) => {
            this._registrationResult = registrationResult;
            this._active = true;
            this._triggerActivation();
        });
    }
    _setupTriggerEventHandlers() {
        this._socketClient.addHandle(constants.triggerEventHandler, (eventIds, eventData) => {
            eventIds.forEach((eventID) => {
                if (!type.isFunction(this._remoteEvents[eventID])) {
                    return;
                }
                this._remoteEvents[eventID](eventData);
            });
        });
    }
    _triggerActivation() {
        this._evtInst.trigger(localEvents.activation);
    }
    isWsRoomSocketClient() {
        return true;
    }
    addHandle(...arg) {
        return this._socketClient.addHandle(...arg);
    }
    removeHandle(name) {
        return this._socketClient.removeHandle(name);
    }
    clearAllHandles() {
        this._socketClient.clearAllHandles();
    }
    on(event, cb) {
        if (this._evtInst.allowedEvent(event)) {
            return Promise.resolve(this._evtInst.on(event, cb));
        }
        else if (this._socketClient.allowedEvent(event)) {
            return Promise.resolve(this._socketClient.on(event, cb));
        }
        return this._socketClient.call(constants.registerEventListenerHandle, event).then((eventID) => {
            this._remoteEvents[eventID] = cb;
            const isEventListenerReference = () => true;
            const deregister = () => {
                this._socketClient.call(constants.deregisterEventListenerHandle, eventID);
                delete this._remoteEvents[eventID];
            };
            return {
                deregister,
                eventID,
                isEventListenerReference,
            };
        });
    }
    trigger(event, eventData) {
        return this._socketClient.call(constants.triggerEventHandler, event, eventData);
    }
    call(...arg) {
        return this._socketClient.call(...arg);
    }
    ask(lookupObj) {
        if (!type.isObject(lookupObj)) {
            throw new Error('The identifier argument should be an object');
        }
        const to = (method, ...args) => {
            const allArgs = [constants.askHandle, lookupObj, method].concat(args);
            return this._socketClient.call(...allArgs);
        };
        return { to };
    }
}

class Identifier extends PropWrap {
    constructor(p = {}) {
        super(p);
    }
    isIdentifier() {
        return true;
    }
}

class WsRoomSocketClientToken extends WsRoomSocketClient {
    constructor(host, token, authObject) {
        super(host, authObject);
        this._token = token;
    }
    _register(...args) {
        this._socketClient.call(constants.requestRoomAccessByKey, this._token, this._authObject).then((registrationResult) => {
            this._registrationResult = registrationResult;
            this._active = true;
            this._triggerActivation();
        }).catch((e) => {
            console.log('unable to register got error:', e);
        });
    }
}

const t$3 = type;
const eventNames = {
    refreshAccessKey: 'wsRoomTokenizer.refreshAccessKey',
    publishAccessKeyCode: 'wsRoomTokenizer.publishAccessKeyCode',
};
class WsRoomTokenizer {
    constructor(wsRoomInst, accessKeyMngrInst, opts = {}) {
        if (!t$3.isObject(wsRoomInst) || t$3.isFunctionable(wsRoomInst.isRoom) !== true) {
            throw new Error('Provide a RoomInstance you twat!!');
        }
        if (!t$3.isObject(accessKeyMngrInst) || t$3.isFunctionable(accessKeyMngrInst.isAccesssKeyMngr) !== true) {
            throw new Error('Provide a RoomInstance you twat!!');
        }
        this._wsRoomInst = wsRoomInst;
        this._accessKeyMngrInst = accessKeyMngrInst;
        this._accessKeyCodes = {};
        this.opts = opts;
        this.opts.accessKey = (!t$3.isObject(this.opts.accessKey)) ? {} : this.opts.accessKey;
        this.opts.accessKey.startsWithAlpha = (!t$3.isBool(this.opts.accessKey.startsWithAlpha)) ? true : this.opts.accessKey.startsWithAlpha;
        this.opts.accessKey.length = (!t$3.isNumber(this.opts.accessKey.length)) ? 4 : this.opts.accessKey.length;
        this.opts.accessKey.ttl = (!t$3.isNumber(this.opts.accessKey.ttl) && this.opts.accessKey.ttl !== false) ? 5 * 60 * 1000 : this.opts.accessKey.ttl;
        this.opts.accessKey.ttd = (!t$3.isNumber(this.opts.accessKey.ttd)) ? 1000 : this.opts.accessKey.ttd;
        this.generateAccessKey();
        this.initRefreshaccessKey();
        wsRoomInst.roomEvents.on('participantJoin', (participant) => {
            participant.wsRoomSocketServerInst.addHandle(constants.requestRoomAccessKey, () => ({ accessKey: this.accessKey }));
        });
    }
    isWsRoomTokenizer() {
        return true;
    }
    initRefreshaccessKey() {
        if (!t$3.isNumber(this.opts.accessKey.ttl)) {
            return;
        }
        setTimeout(() => {
            this.generateAccessKey();
            this.initRefreshaccessKey();
        }, this.opts.accessKey.ttl);
    }
    generateAccessKey() {
        let key = generateToken(this.opts);
        let i = 0;
        while (!this._accessKeyMngrInst.acceptKey(key, this)) {
            key = generateToken(this.opts);
            i++;
            if (i > 100) {
                console.log('more then 100 iterations for retrieving a key');
                break;
            }
        }
        if (t$3.isDefined(this.accessKey)) {
            const oldKey = this.accessKey;
            const dieFn = () => {
                this._accessKeyMngrInst.dropKey(oldKey);
            };
            if (this.opts.accessKey.ttd > 0) {
                setTimeout(dieFn, this.opts.accessKey.ttd);
            }
            else {
                dieFn();
            }
        }
        this.accessKey = key;
        this._wsRoomInst.triggerEvent(false, eventNames.refreshAccessKey, { accessKey: this.accessKey });
    }
    claimKey(accessKey) {
        const code = generateKeyCode();
        this.generateAccessKey();
        this._accessKeyCodes[accessKey] = code;
        console.log('todo, let the accessKeyCode die!!!');
        this._wsRoomInst.triggerEvent(false, eventNames.publishAccessKeyCode, { accessKey });
        return this._wsRoomInst;
    }
}
class AccessKeyMngr {
    constructor() {
        this._activekeys = {};
    }
    isAccesssKeyMngr() {
        return true;
    }
    acceptKey(key, wsRoomTokenizerInst) {
        if (t$3.isDefined(this._activekeys[key])) {
            return false;
        }
        if (!t$3.isObject(wsRoomTokenizerInst) || t$3.isFunctionable(wsRoomTokenizerInst.isWsRoomTokenizer) !== true) {
            throw new Error('Provide a wsRoomTokenizerInst you twat!!');
        }
        this._activekeys[key] = wsRoomTokenizerInst;
        return true;
    }
    dropKey(key) {
        delete this._activekeys[key];
        return true;
    }
    claimKey(key) {
        if (!t$3.isDefined(this._activekeys[key])) {
            return false;
        }
        const roomInstance = this._activekeys[key].claimKey(key);
        delete this._activekeys[key];
        return roomInstance;
    }
}
class WsRoomsTokenizer {
    constructor(wsRoomsInst, opts = {}) {
        this._opts = opts;
        this._opts.roomOpts = (!t$3.isObject(this._opts.roomOpts)) ? {} : this._opts.roomOpts;
        this._accessKeyMngr = new AccessKeyMngr();
        const wsRoomTokenizers = {};
        if (!t$3.isObject(wsRoomsInst) || t$3.isFunctionable(wsRoomsInst.isRooms) !== true) {
            throw new Error('Provide a RoomsInstance you twat!!');
        }
        wsRoomsInst.on('onRoomRegistration', (roomRef) => {
            wsRoomTokenizers[roomRef] = new WsRoomTokenizer(roomRef.getRoom(), this._accessKeyMngr, this._opts.roomOpts);
        });
        wsRoomsInst.on('onRoomRemoval', (roomID) => {
            delete wsRoomTokenizers[roomID];
        });
        wsRoomsInst.on('onConnect', (socketInst) => {
            socketInst.addHandle(constants.requestRoomAccessByKey, (key, authObject) => {
                const roomInstance = this.claimAccessKey(key);
                const denyMsg = 'Unable to register the connection';
                if (roomInstance === false) {
                    console.log('no roomInstance found; implement killing the socket connection');
                    throw new Error(denyMsg);
                }
                let isAccepted = false;
                const acceptCb = () => (isAccepted = true);
                const rejectCb = () => (isAccepted = false);
                return Promise.resolve(this.authMethod(authObject, roomInstance, acceptCb, rejectCb)).then((identifierInst) => {
                    if (isAccepted !== true || !(t$3.isFunctionable(identifierInst.isIdentifier) === true)) {
                        throw new Error(denyMsg);
                    }
                    const matchObject = roomInstance.acceptParticipant(identifierInst, socketInst);
                    if (!t$3.isObject(matchObject) || !t$3.isFunctionable(matchObject.isMatchObject) === true) {
                        throw new Error(denyMsg);
                    }
                    const r = Object.assign({
                        accepted: true,
                        msg: 'You\'re in, in like Flynn'
                    }, matchObject);
                    return JSON.parse(JSON.stringify(r));
                });
            });
        });
    }
    claimAccessKey(key) {
        const roomInstance = this._accessKeyMngr.claimKey(key);
        if (roomInstance === false) {
            return false;
        }
        return roomInstance;
    }
    authMethod(...args) {
        return false;
    }
}

export { helpers, Identifier, WsRoom, WsRooms, WsRoomSocketClient, EventMngr, Factory, SocketClient, SocketServer, WsRoomSocketClientToken, WsRoomsTokenizer };
//# sourceMappingURL=sskutils.js.map
