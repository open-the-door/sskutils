(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('ws')) :
	typeof define === 'function' && define.amd ? define(['exports', 'ws'], factory) :
	(factory((global.sskutils = global.sskutils || {}),global.ws));
}(this, (function (exports,WebSocket$1) { 'use strict';

var Uid = function (prefix) {
    if (prefix === void 0) { prefix = ''; }
    var i = 0;
    var get = function () {
        var v = prefix + i.toString();
        i += 1;
        return v;
    };
    return get;
};
var type = (function () {
    var isDefined = function (v) {
        return typeof v !== 'undefined' && v !== null;
    };
    var isUndefined = function (v) {
        return !isDefined(v);
    };
    var isString = function (v) {
        return isDefined(v) && typeof v === 'string';
    };
    var isObject = function (v) {
        return isDefined(v) && typeof v === 'object' && !isArray(v);
    };
    var isArray = function (v) {
        return Array.isArray(v);
    };
    var isFunction = function (v) {
        return isDefined(v) && typeof v === 'function';
    };
    var isFunctionable = function (f) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        if (!isFunction(f)) {
            return;
        }
        return f.apply({}, args);
    };
    var isBoolean = function (v) {
        return isDefined(v) && typeof v === 'boolean';
    };
    var isBool = isBoolean;
    var isNumber = function (v) {
        return isDefined(v) && typeof v === 'number';
    };
    var forIn = function (o, cb) {
        if (!isObject(o)) {
            return;
        }
        var keys = Object.keys(o);
        keys.forEach(cb);
    };
    var inArray = function (v, arr) {
        return (arr.indexOf(v) >= 0);
    };
    return {
        forIn: forIn,
        inArray: inArray,
        isArray: isArray,
        isBool: isBool,
        isBoolean: isBoolean,
        isDefined: isDefined,
        isFunction: isFunction,
        isFunctionable: isFunctionable,
        isNumber: isNumber,
        isObject: isObject,
        isString: isString,
        isUndefined: isUndefined,
    };
}());
var promiseCache = function () {
    var promises = {};
    var get = function (id) {
        if (type.isDefined(promises[id])) {
            return promises[id].promise;
        }
        var resolve;
        var reject;
        var cb = function (resolveArg, rejectArg) {
            resolve = resolveArg;
            reject = rejectArg;
        };
        var promise = new Promise(cb);
        promises[id] = {
            promise: promise,
            reject: reject,
            resolve: resolve,
        };
        return promises[id].promise;
    };
    var deregister = function (id) {
        if (!type.isDefined(promises[id])) {
            return;
        }
        delete promises[id];
    };
    var resolve = function (id, v) {
        if (!type.isDefined(promises[id])) {
            return;
        }
        promises[id].resolve(v);
        deregister(id);
    };
    var reject = function (id, v) {
        if (!type.isDefined(promises[id])) {
            return;
        }
        promises[id].reject(v);
        deregister(id);
    };
    return {
        get: get,
        reject: reject,
        resolve: resolve
    };
};
var PropWrap = (function () {
    function PropWrap(props) {
        if (!type.isObject(props)) {
            throw new Error('Propwrap is expecting an object babe');
        }
        this._props = props;
    }
    PropWrap.prototype.hasProp = function (k) {
        return type.isDefined(this._props[k]);
    };
    PropWrap.prototype.propIs = function (k, v) {
        return this.hasProp(k) && this.getProp(k) === v;
    };
    PropWrap.prototype.getProp = function (k) {
        if (!this.hasProp(k)) {
            throw new Error('Prop not defined');
        }
        return this._props[k];
    };
    PropWrap.prototype.setProp = function (k, v) {
        this._props[k] = v;
    };
    PropWrap.prototype.serialize = function () {
        return JSON.parse(JSON.stringify(this._props));
    };
    return PropWrap;
}());
var randomizer = (function () {
    var alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
    var numbers = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];
    var chars = alphabet.concat(numbers, numbers);
    var getRandomAlphaNum = function () {
        return alphabet[Math.floor(Math.random() * alphabet.length)];
    };
    var getRandomAlpha = function () {
        return chars[Math.floor(Math.random() * chars.length)];
    };
    var getRandomNumber = function () {
        return (numbers[Math.floor(Math.random() * numbers.length)]).toString();
    };
    return {
        getRandomAlphaNum: getRandomAlphaNum,
        getRandomAlpha: getRandomAlpha,
        getRandomNumber: getRandomNumber,
    };
}());
var generateToken = function (optArg) {
    if (optArg === void 0) { optArg = {}; }
    var typesFactory = {
        alphanumeric: 'getRandomAlphaNum',
        alpha: 'getRandomAlpha',
        numeric: 'getRandomNumber',
    };
    var options = optArg;
    options['length'] = (!type.isNumber(options['length'])) ? 4 : options['length'];
    options['startsWithAlpha'] = (!type.isBool(options['startsWithAlpha'])) ? true : options['startsWithAlpha'];
    options['type'] = (!type.isDefined(typesFactory[options['type']])) ? 'alphanumeric' : options['type'];
    var typeMethod = randomizer[typesFactory[options['type']]];
    var length = options['length'];
    var r = [];
    if (options['startsWithAlpha']) {
        r.push(typeMethod());
        length -= 1;
    }
    for (var i = 0; i < length; i++) {
        r.push(typeMethod());
    }
    return r.join('');
};
var generateKeyCode = function (optArg) {
    if (optArg === void 0) { optArg = {}; }
    var options = optArg;
    options['length'] = (!type.isNumber(options['length'])) ? 4 : options['length'];
    var r = [];
    for (var i = 0; i < options['length']; i++) {
        r.push(randomizer.getRandomNumber());
    }
    return r.join('');
};


var helpers = Object.freeze({
	Uid: Uid,
	type: type,
	promiseCache: promiseCache,
	PropWrap: PropWrap,
	randomizer: randomizer,
	generateToken: generateToken,
	generateKeyCode: generateKeyCode
});

var constants;
(function (constants) {
    constants["askHandle"] = "ask";
    constants["deregisterEventListenerHandle"] = "deregisterEventListenerHandle";
    constants["registerEventListenerHandle"] = "registerEventListenerHandle";
    constants["registrationHandle"] = "register";
    constants["triggerEventHandler"] = "triggerEventHandler";
    constants["requestRoomAccessKey"] = "requestRoomAccessKey";
    constants["requestRoomAccessByKey"] = "requestRoomAccessByKey";
})(constants || (constants = {}));

var eventUid = Uid('X0__EVT__');
var EventMngr = (function () {
    function EventMngr(eventTypes) {
        if (!type.isArray(eventTypes) && !type.isUndefined(eventTypes)) {
            throw new Error('The eventtypes must be undefined or an array');
        }
        this._eventTypes = eventTypes;
        this._listeners = {};
    }
    EventMngr.prototype.addEventType = function (eventType) {
        if (!type.isString(eventType)) {
            throw new Error('EventType should be a string');
        }
        if (!type.isArray(this._eventTypes)) {
            this._eventTypes = [];
        }
        this._eventTypes.push(eventType);
    };
    EventMngr.prototype.allowedEvent = function (event) {
        return type.isUndefined(this._eventTypes) || type.inArray(event, this._eventTypes);
    };
    EventMngr.prototype.on = function (event, cb) {
        var _this = this;
        if (!this.allowedEvent(event)) {
            throw new Error("Provided event not supported; available events: " + (this._eventTypes.join(',')));
        }
        if (type.isUndefined(this._listeners[event])) {
            this._listeners[event] = {};
        }
        var eventID = eventUid();
        this._listeners[event][eventID] = cb;
        var deregister = function () {
            delete _this._listeners[event][eventID];
        };
        var isEventListenerReference = function () { return type.isDefined(_this._listeners[event]) && type.isDefined(_this._listeners[event][eventID]); };
        return {
            deregister: deregister,
            eventID: eventID,
            isEventListenerReference: isEventListenerReference,
        };
    };
    EventMngr.prototype.once = function (event, cb) {
        var deregister;
        var wrappedCb = function (evtData) {
            cb(evtData);
            deregister();
        };
        var r = this.on(event, wrappedCb);
        deregister = r.deregister;
        return r;
    };
    EventMngr.prototype.trigger = function (event, eventData) {
        if (!this.allowedEvent(event)) {
            throw new Error("Provided event not supported; available events: " + this._eventTypes.join(','));
        }
        if (type.isUndefined(this._listeners[event])) {
            return;
        }
        var _eventListeners = this._listeners[event];
        var canceled = false;
        var cancelFn = function () {
            canceled = true;
        };
        type.forIn(_eventListeners, function (eventID) {
            if (!canceled) {
                _eventListeners[eventID](eventData, cancelFn);
            }
        });
    };
    EventMngr.prototype.exposee = function (obj) {
        var _this = this;
        obj.on = function (event, cb) {
            _this.on(event, cb);
        };
        obj.once = function (event, cb) {
            _this.once(event, cb);
        };
    };
    EventMngr.prototype.getEventTypes = function () {
        return this._eventTypes;
    };
    return EventMngr;
}());

var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var c = {
    roomIdKey: 'roomId',
    roomEvents: {
        participantJoin: 'participantJoin',
    }
};
var WsRoom = (function (_super) {
    __extends(WsRoom, _super);
    function WsRoom(proporties) {
        if (proporties === void 0) { proporties = {}; }
        var _this = _super.call(this, proporties) || this;
        _this._participants = {};
        _this._eventUid = {};
        _this._events = {};
        _this._roomEvents = new EventMngr(Object.keys(c.roomEvents));
        _this.roomEvents = {};
        _this._roomEvents.exposee(_this.roomEvents);
        return _this;
    }
    Object.defineProperty(WsRoom.prototype, "roomId", {
        get: function () {
            if (!this.hasProp(c.roomIdKey)) {
                return;
            }
            return this.getProp(c.roomIdKey);
        },
        set: function (v) {
            if (this.hasProp(c.roomIdKey)) {
                throw new Error('Room id can not be overwritten');
            }
            this._participantUid = Uid(v + "X0PART");
            this._eventUid = Uid(v + "X0EVT");
            this.setProp(c.roomIdKey, v);
        },
        enumerable: true,
        configurable: true
    });
    WsRoom.prototype.isRoom = function () {
        return true;
    };
    WsRoom.prototype.acceptParticipant = function (identifierInst, wsRoomSocketServerInst) {
        return this.addParticipant(identifierInst, wsRoomSocketServerInst);
    };
    WsRoom.prototype.findParticipant = function (lookupObj, excludeParticipantId) {
        var _this = this;
        if (excludeParticipantId === void 0) { excludeParticipantId = false; }
        var r = [];
        type.forIn(this._participants, function (pId) {
            var participant = _this._participants[pId];
            if (pId === excludeParticipantId) {
                return;
            }
            var acceptParticipant = true;
            type.forIn(lookupObj, function (attr) {
                if (!participant.identifierInst.propIs(attr, lookupObj[attr])) {
                    acceptParticipant = false;
                }
            });
            if (acceptParticipant) {
                r.push(participant);
            }
        });
        return r;
    };
    WsRoom.prototype.ask = function (participantId, lookupObj, method) {
        var args = [];
        for (var _i = 3; _i < arguments.length; _i++) {
            args[_i - 3] = arguments[_i];
        }
        var participants = this.findParticipant(lookupObj, participantId);
        if (participants.length < 1) {
            var msg = 'Couldnt match any participant';
            throw new Error(msg);
        }
        if (participants.length > 1) {
            return new Promise(function (resolve, reject) {
                participants.forEach(function (participant) {
                    (_a = participant.wsRoomSocketServerInst).call.apply(_a, ([method].concat(args)));
                    var _a;
                });
                resolve('todo should respond with all results');
            });
        }
        var participant = participants[0];
        return (_a = participant.wsRoomSocketServerInst).call.apply(_a, ([method].concat(args))).catch(function (e) {
            throw new Error("Participant replied with error: " + e);
        });
        var _a;
    };
    WsRoom.prototype.registerEventListener = function (participantId, event) {
        var eventID = this._eventUid();
        this._events[eventID] = {
            event: event,
            participantId: participantId,
        };
        return eventID;
    };
    WsRoom.prototype.deregisterEventListener = function (pId, eventID) {
        if (!type.isDefined(this._events[eventID])) {
            return true;
        }
        if (this._events[eventID].participantId !== pId) {
            throw new Error("this event isn't owned by this socket so you can not disable it");
        }
        delete this._events[eventID];
        return true;
    };
    WsRoom.prototype.triggerEvent = function (participantId, event, eventData) {
        var _this = this;
        var participantsEvents = {};
        var hasEvents = false;
        type.forIn(this._events, function (eventID) {
            var eventInst = _this._events[eventID];
            if (eventInst.event !== event || eventInst.participantId === participantId) {
                return;
            }
            if (!type.isArray(participantsEvents[eventInst.participantId])) {
                participantsEvents[eventInst.participantId] = [];
            }
            hasEvents = true;
            participantsEvents[eventInst.participantId].push(eventID);
        });
        if (!hasEvents) {
            return;
        }
        type.forIn(participantsEvents, function (partId) {
            _this._participants[partId].wsRoomSocketServerInst.call(constants.triggerEventHandler, participantsEvents[partId], eventData);
        });
        return;
    };
    WsRoom.prototype.removeEventsForParticipant = function (participantID) {
        var _this = this;
        type.forIn(this._events, function (eventID) {
            if (_this._events[eventID].participantId === participantID) {
                delete _this._events[eventID];
            }
        });
    };
    WsRoom.prototype.removeParticipant = function (participantID) {
        if (!type.isObject(this._participants[participantID])) {
            return;
        }
        this.removeEventsForParticipant(participantID);
        this._participants[participantID].wsRoomSocketServerInst.clearAllHandles();
        this._participants[participantID].wsRoomSocketServerInst.close();
        delete this._participants[participantID];
    };
    WsRoom.prototype.addParticipant = function (identifierInst, wsRoomSocketServerInst) {
        var _this = this;
        if (!this.hasProp(c.roomIdKey)) {
            throw new Error('Room not yet properly initiated');
        }
        var pId = this._participantUid();
        wsRoomSocketServerInst.clearAllHandles();
        wsRoomSocketServerInst.addHandle(constants.askHandle, function (lookupObj, method) {
            var args = [];
            for (var _i = 2; _i < arguments.length; _i++) {
                args[_i - 2] = arguments[_i];
            }
            return _this.ask.apply(_this, [pId, lookupObj, method].concat(args));
        });
        wsRoomSocketServerInst.addHandle(constants.registerEventListenerHandle, function (event) { return _this.registerEventListener(pId, event); });
        wsRoomSocketServerInst.addHandle(constants.deregisterEventListenerHandle, function (eventID) { return _this.deregisterEventListener(pId, eventID); });
        wsRoomSocketServerInst.addHandle(constants.triggerEventHandler, function (event, eventData) { return _this.triggerEvent(pId, event, eventData); });
        wsRoomSocketServerInst.once('close', function () {
            _this.triggerEvent(pId, 'participant.disconnected', identifierInst.serialize());
            return _this.removeParticipant(pId);
        });
        this._participants[pId] = {
            participantId: pId,
            identifierInst: identifierInst,
            wsRoomSocketServerInst: wsRoomSocketServerInst,
        };
        var isMatchObject = function () {
            return true;
        };
        this._roomEvents.trigger(c.roomEvents.participantJoin, this._participants[pId]);
        this.triggerEvent(pId, 'participant.connected', identifierInst.serialize());
        return {
            isMatchObject: isMatchObject,
            participantId: pId,
            roomId: this.roomId,
        };
    };
    return WsRoom;
}(PropWrap));

var messageTypes;
(function (messageTypes) {
    messageTypes["default"] = "default";
    messageTypes["error"] = "error";
    messageTypes["response"] = "response";
    messageTypes["rpc"] = "rpc";
})(messageTypes || (messageTypes = {}));

var Factory = (function () {
    function Factory() {
        this.factories = {};
    }
    Factory.prototype.hasFactoryMethod = function (key) {
        return type.isFunction(this.factories[key]);
    };
    Factory.prototype.addFactoryMethod = function (key, method) {
        var _this = this;
        if (this.hasFactoryMethod(key)) {
            throw new Error(" Factory method with key " + key + " already defined. ");
        }
        if (!type.isFunction(method)) {
            throw new Error(" Factory method " + method + " is not a function. ");
        }
        this.factories[key] = method;
        return {
            delete: function () {
                return _this.deleteFactoryMethod(key);
            },
            getKey: function () {
                return key;
            }
        };
    };
    Factory.prototype.deleteFactoryMethod = function (key) {
        if (!this.hasFactoryMethod(key)) {
            throw new Error(" Factory method with key " + key + " does not exists. ");
        }
        delete this.factories[key];
        return true;
    };
    Factory.prototype.getFactoryMethod = function (key) {
        if (!this.hasFactoryMethod(key)) {
            throw new Error(" Factory method with key " + key + " does not exists. ");
        }
        return this.factories[key];
    };
    Factory.prototype.clear = function () {
        this.factories = {};
    };
    Factory.prototype.handle = function (key, context, argsArg) {
        var method = this.getFactoryMethod(key);
        var args = argsArg;
        if (!type.isArray(argsArg)) {
            args = [argsArg];
        }
        var r = method.apply(context, args);
        if (!type.isDefined(r)) {
            return new Promise(function (resolve) { resolve(); });
        }
        return new Promise(function (resolve, reject) {
            try {
                resolve(r);
            }
            catch (e) {
                reject(e);
            }
        });
    };
    return Factory;
}());

var constants$1 = {
    idKey: 'messageID',
};
var Message = (function () {
    function Message(arg) {
        this.headers = {};
        this.content = {};
        this.type = messageTypes.default;
        if (type.isString(arg)) {
            this.deserialize(arg);
        }
    }
    Message.prototype.isMessage = function () {
        return true;
    };
    Message.prototype.addHeader = function (name, value) {
        this.headers[name] = value;
        return this;
    };
    Message.prototype.getHeader = function (name) {
        return this.headers[name];
    };
    Message.prototype.addContent = function (name, value) {
        this.content[name] = value;
        return this;
    };
    Message.prototype.hasContent = function (name) {
        return type.isDefined(this.content[name]);
    };
    Message.prototype.getContent = function (name) {
        if (!this.hasContent(name)) {
            throw new Error("Content " + name + " ain't defined");
        }
        return this.content[name];
    };
    Message.prototype.setMessageType = function (v) {
        this.type = v;
    };
    Message.prototype.getMessageType = function () {
        return this.type;
    };
    Message.prototype.validate = function () {
        return type.isDefined(this.type) && type.isDefined(this.id);
    };
    Object.defineProperty(Message.prototype, "id", {
        get: function () {
            return this.headers[constants$1.idKey];
        },
        set: function (v) {
            this.headers[constants$1.idKey] = v;
        },
        enumerable: true,
        configurable: true
    });
    Message.prototype.serialize = function () {
        if (!this.validate()) {
            throw new Error('invalid message, unable to serialize.');
        }
        var o = {
            content: this.content,
            headers: this.headers,
            type: this.type,
        };
        return JSON.stringify(o);
    };
    Message.prototype.deserialize = function (serializedContent) {
        var _this = this;
        if (!type.isString(serializedContent)) {
            throw new Error('Desirialisation can only be done from a string');
        }
        var o = JSON.parse(serializedContent);
        if (!type.isDefined(o.type)) {
            throw new Error('Not a proper message object, missing a type');
        }
        this.setMessageType(o.type);
        if (type.isObject(o.headers)) {
            type.forIn(o.headers, function (v) {
                _this.addHeader(v, o.headers[v]);
            });
        }
        if (type.isObject(o.content)) {
            type.forIn(o.content, function (v) {
                _this.addContent(v, o.content[v]);
            });
        }
        return true;
    };
    Message.prototype.generateReply = function () {
        var arg = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            arg[_i] = arguments[_i];
        }
        return false;
    };
    return Message;
}());

var getMessageType = function (arg) {
    if (!type.isString(arg)) {
        return;
    }
    try {
        var o = JSON.parse(arg);
        return o.type;
    }
    catch (e) {
        return;
    }
};

var __extends$3 = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var constants$2 = {
    answerName: 'answerName',
    argumentsName: 'argumentsName',
    errorName: 'errorName',
    initiatorIdName: 'initiatorIdName',
    procedureName: 'procedureName',
};
var RpcMessage = (function (_super) {
    __extends$3(RpcMessage, _super);
    function RpcMessage(arg) {
        var _this = _super.call(this, arg) || this;
        _this.type = messageTypes.rpc;
        return _this;
    }
    RpcMessage.prototype.expectResponse = function () {
        return true;
    };
    RpcMessage.prototype.validate = function () {
        if (!_super.prototype.hasContent.call(this, constants$2.procedureName)) {
            return false;
        }
        return _super.prototype.validate.call(this);
    };
    Object.defineProperty(RpcMessage.prototype, "procedure", {
        get: function () {
            return _super.prototype.getContent.call(this, constants$2.procedureName);
        },
        set: function (procedureName) {
            _super.prototype.addContent.call(this, constants$2.procedureName, procedureName);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RpcMessage.prototype, "arguments", {
        get: function () {
            return _super.prototype.getContent.call(this, constants$2.argumentsName);
        },
        set: function (args) {
            _super.prototype.addContent.call(this, constants$2.argumentsName, args);
        },
        enumerable: true,
        configurable: true
    });
    RpcMessage.prototype.generateReply = function (messageId, answer) {
        var responseMessageInst = new ResponseMessage(_super.prototype.serialize.call(this));
        responseMessageInst.initiatorId = this.id;
        responseMessageInst.id = messageId;
        responseMessageInst.answer = answer;
        return responseMessageInst;
    };
    RpcMessage.prototype.generateError = function (messageId, error) {
        var errorMessageInst = new ErrorMessage(_super.prototype.serialize.call(this));
        errorMessageInst.initiatorId = this.id;
        errorMessageInst.id = messageId;
        errorMessageInst.error = error;
        return errorMessageInst;
    };
    RpcMessage.prototype.addContent = function (name, value) {
        if (name === constants$2.procedureName) {
            this.procedure = value;
            return value;
        }
        else if (name === constants$2.argumentsName) {
            this.arguments = value;
            return value;
        }
        return _super.prototype.addContent.call(this, name, value);
    };
    return RpcMessage;
}(Message));
var ResponseMessage = (function (_super) {
    __extends$3(ResponseMessage, _super);
    function ResponseMessage(arg) {
        var _this = _super.call(this, arg) || this;
        _this.type = messageTypes.response;
        return _this;
    }
    ResponseMessage.prototype.expectResponse = function () {
        return true;
    };
    ResponseMessage.prototype.validate = function () {
        if (!_super.prototype.hasContent.call(this, constants$2.initiatorIdName) || !_super.prototype.hasContent.call(this, constants$2.answerName)) {
            return false;
        }
        return _super.prototype.validate.call(this);
    };
    Object.defineProperty(ResponseMessage.prototype, "initiatorId", {
        get: function () {
            return _super.prototype.getContent.call(this, constants$2.initiatorIdName);
        },
        set: function (v) {
            _super.prototype.addContent.call(this, constants$2.initiatorIdName, v);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ResponseMessage.prototype, "answer", {
        get: function () {
            return _super.prototype.getContent.call(this, constants$2.answerName);
        },
        set: function (v) {
            _super.prototype.addContent.call(this, constants$2.answerName, v);
        },
        enumerable: true,
        configurable: true
    });
    ResponseMessage.prototype.addContent = function (name, value) {
        if (name === constants$2.initiatorIdName) {
            this.initiatorId = value;
            return value;
        }
        return _super.prototype.addContent.call(this, name, value);
    };
    ResponseMessage.prototype.generateReply = function () {
        return false;
    };
    ResponseMessage.prototype.generateError = function () {
        return false;
    };
    return ResponseMessage;
}(RpcMessage));
var ErrorMessage = (function (_super) {
    __extends$3(ErrorMessage, _super);
    function ErrorMessage(arg) {
        var _this = _super.call(this, arg) || this;
        _this.type = messageTypes.error;
        return _this;
    }
    ErrorMessage.prototype.expectResponse = function () {
        return true;
    };
    ErrorMessage.prototype.validate = function () {
        if (!this.hasContent(constants$2.initiatorIdName) || !this.hasContent(constants$2.errorName)) {
            return false;
        }
        return _super.prototype.validate.call(this);
    };
    Object.defineProperty(ErrorMessage.prototype, "initiatorId", {
        get: function () {
            return _super.prototype.getContent.call(this, constants$2.initiatorIdName);
        },
        set: function (v) {
            _super.prototype.addContent.call(this, constants$2.initiatorIdName, v);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ErrorMessage.prototype, "error", {
        get: function () {
            return _super.prototype.getContent.call(this, constants$2.errorName);
        },
        set: function (v) {
            _super.prototype.addContent.call(this, constants$2.errorName, v);
        },
        enumerable: true,
        configurable: true
    });
    ErrorMessage.prototype.addContent = function (name, value) {
        if (name === constants$2.initiatorIdName) {
            this.initiatorId = value;
            return value;
        }
        return _super.prototype.addContent.call(this, name, value);
    };
    ErrorMessage.prototype.generateReply = function () {
        return false;
    };
    ErrorMessage.prototype.generateError = function () {
        return false;
    };
    return ErrorMessage;
}(RpcMessage));

var t$1 = type;
var eventUid$1 = Uid('X0SCKABST__EVT__');
var messageUid = Uid('X0SCKABST__MSG__');
var socketEvents = {
    message: 'message',
    close: 'close',
    error: 'error',
    open: 'open',
};
var messageTypeClasses = {};
messageTypeClasses[messageTypes.rpc] = RpcMessage;
messageTypeClasses[messageTypes.response] = ResponseMessage;
messageTypeClasses[messageTypes.error] = ErrorMessage;
var SocketAbstract = (function () {
    function SocketAbstract() {
        var _this = this;
        this._eventListeners = {};
        this._messageFactory = new Factory();
        this._rpcPromiseHandler = promiseCache();
        this.on(socketEvents.message, function (msgString, cancelFn, replyFn, catchFn) {
            try {
                var result = _this._runMsgFactory(msgString);
                if (result instanceof Promise) {
                    result.then(function (r) {
                        replyFn(r);
                    });
                    result.catch(function (e) {
                        catchFn(e);
                    });
                }
            }
            catch (e) {
                catchFn(e);
            }
        });
    }
    SocketAbstract.prototype.addHandle = function (name, method) {
        if (!t$1.isString(name)) {
            throw new Error('Adding a handle requires a handle name');
        }
        if (this._messageFactory.hasFactoryMethod(name)) {
            throw new Error("The handle " + name + " has already been declared or is reserved ");
        }
        return this._messageFactory.addFactoryMethod(name, method);
    };
    SocketAbstract.prototype.removeHandle = function (name) {
        if (this._messageFactory.hasFactoryMethod(name)) {
            return this._messageFactory.deleteFactoryMethod(name);
        }
        return true;
    };
    SocketAbstract.prototype.clearAllHandles = function () {
        this._messageFactory.clear();
    };
    SocketAbstract.prototype._runMsgFactory = function (msgString) {
        var msgType = getMessageType(msgString);
        if (!t$1.isDefined(msgType)) {
            return;
        }
        var msgInstance = new messageTypeClasses[msgType](msgString);
        if (msgType === messageTypes.rpc) {
            return this._messageFactory.handle(msgInstance.procedure, this, msgInstance.arguments);
        }
        else if (msgType === messageTypes.response) {
            this._rpcPromiseHandler.resolve(msgInstance.initiatorId, msgInstance.answer);
        }
        else if (msgType === messageTypes.error) {
            this._rpcPromiseHandler.reject(msgInstance.initiatorId, msgInstance.error);
        }
    };
    SocketAbstract.prototype.on = function (event, cb) {
        var _this = this;
        if (!t$1.isDefined(socketEvents[event])) {
            throw new Error("Event " + event + " unavailable");
        }
        if (!t$1.isDefined(this._eventListeners[event])) {
            this._eventListeners[event] = {};
        }
        var eventId = eventUid$1();
        this._eventListeners[event][eventId] = cb;
        var deregister = function () {
            delete _this._eventListeners[event][eventId];
        };
        return {
            eventId: eventId,
            deregister: deregister
        };
    };
    SocketAbstract.prototype.once = function (event, cb) {
        var deregister;
        var wrappedCb = function (evtData) {
            cb(evtData);
            deregister();
        };
        var r = this.on(event, wrappedCb);
        deregister = r.deregister;
        return r;
    };
    SocketAbstract.prototype._trigger = function (event, eventData) {
        var _this = this;
        if (!t$1.isDefined(socketEvents[event])) {
            throw new Error("Event " + event + " unavailable");
        }
        var eventListeners = this._eventListeners[event];
        var canceled = false;
        var cancelFn = function () {
            canceled = true;
        };
        var replyFn = function () {
            var arg = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                arg[_i] = arguments[_i];
            }
        };
        var catchFn = function () {
            var arg = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                arg[_i] = arguments[_i];
            }
        };
        if (event === socketEvents.message) {
            replyFn = function (answer) {
                var msgInst = new messageTypeClasses[getMessageType(eventData)](eventData);
                if (t$1.isDefined(answer)) {
                    var answerInst = msgInst.generateReply(messageUid(), answer);
                    if (answerInst !== false) {
                        _this.send(answerInst);
                    }
                }
            };
            catchFn = function (error) {
                var msgInst = new messageTypeClasses[getMessageType(eventData)](eventData);
                var errorInst = msgInst.generateError(messageUid(), error.message);
                if (errorInst !== false) {
                    _this.send(errorInst);
                }
            };
        }
        t$1.forIn(eventListeners, function (listener) {
            if (!canceled) {
                eventListeners[listener](eventData, cancelFn, replyFn, catchFn);
            }
        });
    };
    SocketAbstract.prototype.getMessage = function (messageType) {
        if (!t$1.isDefined(messageTypeClasses[messageType])) {
            throw new Error("The messagetype " + messageType + " does not exists what are you thinking!");
        }
        var msgInstance = new messageTypeClasses[messageType]();
        msgInstance.id = messageUid();
        return msgInstance;
    };
    SocketAbstract.prototype._send = function () {
        var arg = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            arg[_i] = arguments[_i];
        }
        throw new Error('And this is something that the implementor should have overwritten!!!!');
    };
    SocketAbstract.prototype.close = function () {
        var arg = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            arg[_i] = arguments[_i];
        }
        throw new Error('And this is something that the implementor should have overwritten!!!!');
    };
    SocketAbstract.prototype.send = function (msgInst) {
        if (!(msgInst instanceof Message)) {
            throw new Error('Provide instance of Message to send, you a-hole');
        }
        this._send(msgInst.serialize());
    };
    SocketAbstract.prototype.call = function (procedure) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        var rpcMsg = this.getMessage('rpc');
        rpcMsg.procedure = procedure;
        rpcMsg.arguments = args;
        var p = this._rpcPromiseHandler.get(rpcMsg.id);
        this.send(rpcMsg);
        return p;
    };
    SocketAbstract.prototype.getEventTypes = function () {
        return Object.keys(socketEvents);
    };
    SocketAbstract.prototype.allowedEvent = function (event) {
        return t$1.isDefined(socketEvents[event]);
    };
    return SocketAbstract;
}());

var __extends$2 = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var SocketServer = (function (_super) {
    __extends$2(SocketServer, _super);
    function SocketServer(wsSocket) {
        var _this = _super.call(this) || this;
        _this._wsSocket = wsSocket;
        _this._wsSocket.on('message', function (evt) {
            _super.prototype._trigger.call(_this, 'message', evt);
        });
        _this._wsSocket.on('close', function (evt) {
            _super.prototype._trigger.call(_this, 'close', evt);
        });
        return _this;
    }
    SocketServer.prototype._send = function (msgString) {
        this._wsSocket.send(msgString);
    };
    SocketServer.prototype.close = function () {
        this._wsSocket.close();
    };
    return SocketServer;
}(SocketAbstract));

var __extends$1 = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var WsRoomSocketServer = (function (_super) {
    __extends$1(WsRoomSocketServer, _super);
    function WsRoomSocketServer() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    WsRoomSocketServer.prototype.isWsRoomSocketServer = function () {
        return true;
    };
    return WsRoomSocketServer;
}(SocketServer));

var WebSocketServer = WebSocket$1.Server;
var t = type;
var roomUid = Uid('X0ROOM');
var events = {
    onConnect: 'onConnect',
    onRegistration: 'onRegistration',
    onRoomRegistration: 'onRoomRegistration',
    onRoomRemoval: 'onRoomRemoval',
};
var WsRooms = (function () {
    function WsRooms(httpServer) {
        var _this = this;
        this._wsServer = new WebSocketServer({ server: httpServer });
        this._events = new EventMngr(Object.keys(events));
        this._events.exposee(this);
        this._rooms = {};
        this._props = {};
        this._wsServer.on('connection', function (socketArg) {
            _this._onConnect(socketArg);
        });
        this.authMethod = function (authObj, acceptCb) {
            acceptCb();
        };
    }
    WsRooms.prototype.isRooms = function () {
        return true;
    };
    WsRooms.prototype._onConnect = function (socketArg) {
        var _this = this;
        var socketInst = new WsRoomSocketServer(socketArg);
        this._events.trigger(events.onConnect, socketInst);
        socketInst.addHandle(constants.registrationHandle, function (authObj) {
            var isAccepted = false;
            var identifierInst = false;
            var accept = function () { return (isAccepted = true); };
            var reject = function () { return (isAccepted = false); };
            return Promise.resolve(_this.authMethod(authObj, accept, reject)).then(function (identifierInstArg) {
                identifierInst = (t.isFunctionable(identifierInstArg.isIdentifier) === true) ? identifierInstArg : false;
                if (isAccepted !== true) {
                    throw new Error('Unable to register the connection');
                }
                return { accepted: true, msg: 'You\'re in, in like Flynn' };
            }).then(function (rArg) {
                var r = rArg;
                if (rArg.accepted === true) {
                    var match = _this._matchRoom(identifierInst, socketInst);
                    if (t.isObject(match) && type.isFunctionable(match['isMatchObject']) === true) {
                        r = Object.assign(r, match);
                        delete r['isMatchObject'];
                    }
                }
                return r;
            }).catch(function () {
                console.log('nope mate that plane won\'t go up');
            });
        });
    };
    WsRooms.prototype._matchRoom = function (identifierInst, socketInst) {
        var _this = this;
        var match = false;
        type.forIn(this._rooms, function (roomId) {
            var matchObject = _this._rooms[roomId].acceptParticipant(identifierInst, socketInst);
            if (!match && type.isObject(matchObject) && type.isFunctionable(matchObject.isMatchObject) === true) {
                match = matchObject;
            }
        });
        return match;
    };
    Object.defineProperty(WsRooms.prototype, "authMethod", {
        get: function () {
            return this._props.authMethod;
        },
        set: function (v) {
            if (!t.isFunction(v)) {
                throw new Error('The authentication method must be a function');
            }
            this._props.authMethod = v;
        },
        enumerable: true,
        configurable: true
    });
    WsRooms.prototype.addRoom = function (roomInst) {
        var _this = this;
        var roomID = roomUid();
        roomInst.roomId = roomID;
        if (!t.isFunction(roomInst.isRoom) || roomInst.isRoom() !== true) {
            throw new Error('Provided argument should be an instance of the type Room.');
        }
        this._rooms[roomID] = roomInst;
        var deregister = function () {
            _this._events.trigger(events.onRoomRemoval, roomID);
            delete _this._rooms[roomID];
        };
        var isRoomReference = function () { return type.isDefined(_this._rooms[roomID]); };
        var getRoom = function () { return _this._rooms[roomID]; };
        var result = {
            deregister: deregister,
            getRoom: getRoom,
            isRoomRef: isRoomReference,
            isRoomReference: isRoomReference,
            roomID: roomID,
        };
        this._events.trigger(events.onRoomRegistration, result);
        return result;
    };
    return WsRooms;
}());

var __extends$4 = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var t$2 = type;
var SocketClient = (function (_super) {
    __extends$4(SocketClient, _super);
    function SocketClient(host) {
        var _this = _super.call(this) || this;
        _this._conf = {
            keepAlive: false
        };
        if (!t$2.isString(host)) {
            throw new Error('Please provide a proper host');
        }
        _this.host = host;
        _this.running = false;
        _this.setup();
        return _this;
    }
    Object.defineProperty(SocketClient.prototype, "keepAlive", {
        get: function () {
            return this._conf.keepAlive;
        },
        set: function (v) {
            if (!t$2.isBool(v)) {
                throw new Error('KeepAlive should be numeric value');
            }
            this._conf.keepAlive = v;
        },
        enumerable: true,
        configurable: true
    });
    SocketClient.prototype.setup = function () {
        var _this = this;
        this._socket = new WebSocket(this.host);
        this.running = true;
        this._socket.onopen = function (evt) {
            _this._trigger('open', evt);
        };
        this._socket.onmessage = function (evt) {
            _this._trigger('message', evt.data);
        };
        this._socket.onclose = function () {
            _this.onClose();
            _this._trigger('close', {});
        };
        this._socket.onerror = function (evt) {
            _this._trigger('error', evt);
        };
    };
    SocketClient.prototype.onClose = function () {
        this.running = false;
        if (this._conf.keepAlive) {
            this.setup();
        }
    };
    SocketClient.prototype.isRunning = function () {
        return this.running;
    };
    SocketClient.prototype._send = function (msgString) {
        this._socket.send(msgString);
    };
    return SocketClient;
}(SocketAbstract));

var localEvents = {
    activation: 'activation',
};
var WsRoomSocketClient = (function () {
    function WsRoomSocketClient(host, authObject) {
        var _this = this;
        this._evtInst = new EventMngr(Object.keys(localEvents));
        this._authObject = authObject;
        this._registrationResult = {};
        this._active = false;
        this._remoteEvents = {};
        this._socketClient = new SocketClient(host);
        this._socketClient.once('open', function () {
            _this._register();
            _this._setupTriggerEventHandlers();
        });
    }
    WsRoomSocketClient.prototype._register = function () {
        var _this = this;
        this._socketClient.call(constants.registrationHandle, this._authObject).then(function (registrationResult) {
            _this._registrationResult = registrationResult;
            _this._active = true;
            _this._triggerActivation();
        });
    };
    WsRoomSocketClient.prototype._setupTriggerEventHandlers = function () {
        var _this = this;
        this._socketClient.addHandle(constants.triggerEventHandler, function (eventIds, eventData) {
            eventIds.forEach(function (eventID) {
                if (!type.isFunction(_this._remoteEvents[eventID])) {
                    return;
                }
                _this._remoteEvents[eventID](eventData);
            });
        });
    };
    WsRoomSocketClient.prototype._triggerActivation = function () {
        this._evtInst.trigger(localEvents.activation);
    };
    WsRoomSocketClient.prototype.isWsRoomSocketClient = function () {
        return true;
    };
    WsRoomSocketClient.prototype.addHandle = function () {
        var arg = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            arg[_i] = arguments[_i];
        }
        return (_a = this._socketClient).addHandle.apply(_a, arg);
        var _a;
    };
    WsRoomSocketClient.prototype.removeHandle = function (name) {
        return this._socketClient.removeHandle(name);
    };
    WsRoomSocketClient.prototype.clearAllHandles = function () {
        this._socketClient.clearAllHandles();
    };
    WsRoomSocketClient.prototype.on = function (event, cb) {
        var _this = this;
        if (this._evtInst.allowedEvent(event)) {
            return Promise.resolve(this._evtInst.on(event, cb));
        }
        else if (this._socketClient.allowedEvent(event)) {
            return Promise.resolve(this._socketClient.on(event, cb));
        }
        return this._socketClient.call(constants.registerEventListenerHandle, event).then(function (eventID) {
            _this._remoteEvents[eventID] = cb;
            var isEventListenerReference = function () { return true; };
            var deregister = function () {
                _this._socketClient.call(constants.deregisterEventListenerHandle, eventID);
                delete _this._remoteEvents[eventID];
            };
            return {
                deregister: deregister,
                eventID: eventID,
                isEventListenerReference: isEventListenerReference,
            };
        });
    };
    WsRoomSocketClient.prototype.trigger = function (event, eventData) {
        return this._socketClient.call(constants.triggerEventHandler, event, eventData);
    };
    WsRoomSocketClient.prototype.call = function () {
        var arg = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            arg[_i] = arguments[_i];
        }
        return (_a = this._socketClient).call.apply(_a, arg);
        var _a;
    };
    WsRoomSocketClient.prototype.ask = function (lookupObj) {
        var _this = this;
        if (!type.isObject(lookupObj)) {
            throw new Error('The identifier argument should be an object');
        }
        var to = function (method) {
            var args = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                args[_i - 1] = arguments[_i];
            }
            var allArgs = [constants.askHandle, lookupObj, method].concat(args);
            return (_a = _this._socketClient).call.apply(_a, allArgs);
            var _a;
        };
        return { to: to };
    };
    return WsRoomSocketClient;
}());

var __extends$5 = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Identifier = (function (_super) {
    __extends$5(Identifier, _super);
    function Identifier(p) {
        if (p === void 0) { p = {}; }
        return _super.call(this, p) || this;
    }
    Identifier.prototype.isIdentifier = function () {
        return true;
    };
    return Identifier;
}(PropWrap));

var __extends$6 = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var WsRoomSocketClientToken = (function (_super) {
    __extends$6(WsRoomSocketClientToken, _super);
    function WsRoomSocketClientToken(host, token, authObject) {
        var _this = _super.call(this, host, authObject) || this;
        _this._token = token;
        return _this;
    }
    WsRoomSocketClientToken.prototype._register = function () {
        var _this = this;
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        this._socketClient.call(constants.requestRoomAccessByKey, this._token, this._authObject).then(function (registrationResult) {
            _this._registrationResult = registrationResult;
            _this._active = true;
            _this._triggerActivation();
        }).catch(function (e) {
            console.log('unable to register got error:', e);
        });
    };
    return WsRoomSocketClientToken;
}(WsRoomSocketClient));

var t$3 = type;
var eventNames = {
    refreshAccessKey: 'wsRoomTokenizer.refreshAccessKey',
    publishAccessKeyCode: 'wsRoomTokenizer.publishAccessKeyCode',
};
var WsRoomTokenizer = (function () {
    function WsRoomTokenizer(wsRoomInst, accessKeyMngrInst, opts) {
        if (opts === void 0) { opts = {}; }
        var _this = this;
        if (!t$3.isObject(wsRoomInst) || t$3.isFunctionable(wsRoomInst.isRoom) !== true) {
            throw new Error('Provide a RoomInstance you twat!!');
        }
        if (!t$3.isObject(accessKeyMngrInst) || t$3.isFunctionable(accessKeyMngrInst.isAccesssKeyMngr) !== true) {
            throw new Error('Provide a RoomInstance you twat!!');
        }
        this._wsRoomInst = wsRoomInst;
        this._accessKeyMngrInst = accessKeyMngrInst;
        this._accessKeyCodes = {};
        this.opts = opts;
        this.opts.accessKey = (!t$3.isObject(this.opts.accessKey)) ? {} : this.opts.accessKey;
        this.opts.accessKey.startsWithAlpha = (!t$3.isBool(this.opts.accessKey.startsWithAlpha)) ? true : this.opts.accessKey.startsWithAlpha;
        this.opts.accessKey.length = (!t$3.isNumber(this.opts.accessKey.length)) ? 4 : this.opts.accessKey.length;
        this.opts.accessKey.ttl = (!t$3.isNumber(this.opts.accessKey.ttl) && this.opts.accessKey.ttl !== false) ? 5 * 60 * 1000 : this.opts.accessKey.ttl;
        this.opts.accessKey.ttd = (!t$3.isNumber(this.opts.accessKey.ttd)) ? 1000 : this.opts.accessKey.ttd;
        this.generateAccessKey();
        this.initRefreshaccessKey();
        wsRoomInst.roomEvents.on('participantJoin', function (participant) {
            participant.wsRoomSocketServerInst.addHandle(constants.requestRoomAccessKey, function () { return ({ accessKey: _this.accessKey }); });
        });
    }
    WsRoomTokenizer.prototype.isWsRoomTokenizer = function () {
        return true;
    };
    WsRoomTokenizer.prototype.initRefreshaccessKey = function () {
        var _this = this;
        if (!t$3.isNumber(this.opts.accessKey.ttl)) {
            return;
        }
        setTimeout(function () {
            _this.generateAccessKey();
            _this.initRefreshaccessKey();
        }, this.opts.accessKey.ttl);
    };
    WsRoomTokenizer.prototype.generateAccessKey = function () {
        var _this = this;
        var key = generateToken(this.opts);
        var i = 0;
        while (!this._accessKeyMngrInst.acceptKey(key, this)) {
            key = generateToken(this.opts);
            i++;
            if (i > 100) {
                console.log('more then 100 iterations for retrieving a key');
                break;
            }
        }
        if (t$3.isDefined(this.accessKey)) {
            var oldKey_1 = this.accessKey;
            var dieFn = function () {
                _this._accessKeyMngrInst.dropKey(oldKey_1);
            };
            if (this.opts.accessKey.ttd > 0) {
                setTimeout(dieFn, this.opts.accessKey.ttd);
            }
            else {
                dieFn();
            }
        }
        this.accessKey = key;
        this._wsRoomInst.triggerEvent(false, eventNames.refreshAccessKey, { accessKey: this.accessKey });
    };
    WsRoomTokenizer.prototype.claimKey = function (accessKey) {
        var code = generateKeyCode();
        this.generateAccessKey();
        this._accessKeyCodes[accessKey] = code;
        console.log('todo, let the accessKeyCode die!!!');
        this._wsRoomInst.triggerEvent(false, eventNames.publishAccessKeyCode, { accessKey: accessKey });
        return this._wsRoomInst;
    };
    return WsRoomTokenizer;
}());
var AccessKeyMngr = (function () {
    function AccessKeyMngr() {
        this._activekeys = {};
    }
    AccessKeyMngr.prototype.isAccesssKeyMngr = function () {
        return true;
    };
    AccessKeyMngr.prototype.acceptKey = function (key, wsRoomTokenizerInst) {
        if (t$3.isDefined(this._activekeys[key])) {
            return false;
        }
        if (!t$3.isObject(wsRoomTokenizerInst) || t$3.isFunctionable(wsRoomTokenizerInst.isWsRoomTokenizer) !== true) {
            throw new Error('Provide a wsRoomTokenizerInst you twat!!');
        }
        this._activekeys[key] = wsRoomTokenizerInst;
        return true;
    };
    AccessKeyMngr.prototype.dropKey = function (key) {
        delete this._activekeys[key];
        return true;
    };
    AccessKeyMngr.prototype.claimKey = function (key) {
        if (!t$3.isDefined(this._activekeys[key])) {
            return false;
        }
        var roomInstance = this._activekeys[key].claimKey(key);
        delete this._activekeys[key];
        return roomInstance;
    };
    return AccessKeyMngr;
}());
var WsRoomsTokenizer = (function () {
    function WsRoomsTokenizer(wsRoomsInst, opts) {
        if (opts === void 0) { opts = {}; }
        var _this = this;
        this._opts = opts;
        this._opts.roomOpts = (!t$3.isObject(this._opts.roomOpts)) ? {} : this._opts.roomOpts;
        this._accessKeyMngr = new AccessKeyMngr();
        var wsRoomTokenizers = {};
        if (!t$3.isObject(wsRoomsInst) || t$3.isFunctionable(wsRoomsInst.isRooms) !== true) {
            throw new Error('Provide a RoomsInstance you twat!!');
        }
        wsRoomsInst.on('onRoomRegistration', function (roomRef) {
            wsRoomTokenizers[roomRef] = new WsRoomTokenizer(roomRef.getRoom(), _this._accessKeyMngr, _this._opts.roomOpts);
        });
        wsRoomsInst.on('onRoomRemoval', function (roomID) {
            delete wsRoomTokenizers[roomID];
        });
        wsRoomsInst.on('onConnect', function (socketInst) {
            socketInst.addHandle(constants.requestRoomAccessByKey, function (key, authObject) {
                var roomInstance = _this.claimAccessKey(key);
                var denyMsg = 'Unable to register the connection';
                if (roomInstance === false) {
                    console.log('no roomInstance found; implement killing the socket connection');
                    throw new Error(denyMsg);
                }
                var isAccepted = false;
                var acceptCb = function () { return (isAccepted = true); };
                var rejectCb = function () { return (isAccepted = false); };
                return Promise.resolve(_this.authMethod(authObject, roomInstance, acceptCb, rejectCb)).then(function (identifierInst) {
                    if (isAccepted !== true || !(t$3.isFunctionable(identifierInst.isIdentifier) === true)) {
                        throw new Error(denyMsg);
                    }
                    var matchObject = roomInstance.acceptParticipant(identifierInst, socketInst);
                    if (!t$3.isObject(matchObject) || !t$3.isFunctionable(matchObject.isMatchObject) === true) {
                        throw new Error(denyMsg);
                    }
                    var r = Object.assign({
                        accepted: true,
                        msg: 'You\'re in, in like Flynn'
                    }, matchObject);
                    return JSON.parse(JSON.stringify(r));
                });
            });
        });
    }
    WsRoomsTokenizer.prototype.claimAccessKey = function (key) {
        var roomInstance = this._accessKeyMngr.claimKey(key);
        if (roomInstance === false) {
            return false;
        }
        return roomInstance;
    };
    WsRoomsTokenizer.prototype.authMethod = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        return false;
    };
    return WsRoomsTokenizer;
}());

exports.helpers = helpers;
exports.Identifier = Identifier;
exports.WsRoom = WsRoom;
exports.WsRooms = WsRooms;
exports.WsRoomSocketClient = WsRoomSocketClient;
exports.EventMngr = EventMngr;
exports.Factory = Factory;
exports.SocketClient = SocketClient;
exports.SocketServer = SocketServer;
exports.WsRoomSocketClientToken = WsRoomSocketClientToken;
exports.WsRoomsTokenizer = WsRoomsTokenizer;

Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=sskutils.umd.js.map
