export declare const Uid: (prefix?: string) => () => string;
export declare const type: {
    forIn: (o: any, cb: any) => void;
    inArray: (v: any, arr: any) => boolean;
    isArray: (v: any) => boolean;
    isBool: (v: any) => boolean;
    isBoolean: (v: any) => boolean;
    isDefined: (v: any) => boolean;
    isFunction: (v: any) => boolean;
    isFunctionable: (f: any, ...args: any[]) => any;
    isNumber: (v: any) => boolean;
    isObject: (v: any) => boolean;
    isString: (v: any) => boolean;
    isUndefined: (v: any) => boolean;
};
export declare const promiseCache: () => {
    get: (id: any) => any;
    reject: (id: any, v: any) => void;
    resolve: (id: any, v: any) => void;
};
export declare class PropWrap {
    protected _props: any;
    constructor(props: any);
    hasProp(k: any): boolean;
    propIs(k: any, v: any): boolean;
    getProp(k: any): any;
    setProp(k: any, v: any): void;
    serialize(): any;
}
export declare const randomizer: {
    getRandomAlphaNum: () => string;
    getRandomAlpha: () => string;
    getRandomNumber: () => string;
};
export declare const generateToken: (optArg?: {}) => string;
export declare const generateKeyCode: (optArg?: {}) => string;
