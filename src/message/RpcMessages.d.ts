import { Message } from './Message';
export declare class RpcMessage extends Message {
    constructor(arg: any);
    expectResponse(): boolean;
    validate(): boolean;
    procedure: any;
    arguments: any;
    generateReply(messageId: any, answer: any): any;
    generateError(messageId: any, error: any): any;
    addContent(name: any, value: any): any;
}
export declare class ResponseMessage extends RpcMessage {
    constructor(arg: any);
    expectResponse(): boolean;
    validate(): boolean;
    initiatorId: any;
    answer: any;
    addContent(name: any, value: any): any;
    generateReply(): boolean;
    generateError(): boolean;
}
export declare class ErrorMessage extends RpcMessage {
    id: any;
    constructor(arg: any);
    expectResponse(): boolean;
    validate(): boolean;
    initiatorId: any;
    error: any;
    addContent(name: any, value: any): any;
    generateReply(): boolean;
    generateError(): boolean;
}
