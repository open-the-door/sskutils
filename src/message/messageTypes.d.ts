export declare enum messageTypes {
    default = "default",
    error = "error",
    response = "response",
    rpc = "rpc",
}
