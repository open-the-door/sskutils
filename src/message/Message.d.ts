export declare class Message {
    private headers;
    private content;
    protected type: any;
    constructor(arg: any);
    isMessage(): boolean;
    addHeader(name: any, value: any): this;
    getHeader(name: any): any;
    addContent(name: any, value: any): this;
    hasContent(name: any): boolean;
    getContent(name: any): any;
    setMessageType(v: any): void;
    getMessageType(): any;
    validate(): boolean;
    id: any;
    serialize(): string;
    deserialize(serializedContent: any): boolean;
    generateReply(...arg: any[]): boolean;
}
