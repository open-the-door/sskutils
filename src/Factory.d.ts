export default class Factory {
    protected factories: any;
    constructor();
    hasFactoryMethod(key: any): boolean;
    addFactoryMethod(key: any, method: any): {
        delete: () => boolean;
        getKey: () => any;
    };
    deleteFactoryMethod(key: any): boolean;
    getFactoryMethod(key: any): any;
    clear(): void;
    handle(key: any, context: any, argsArg: any): Promise<{}>;
}
