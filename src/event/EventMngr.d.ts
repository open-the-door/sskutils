export default class EventMngr {
    protected _eventTypes: any;
    protected _listeners: any;
    constructor(eventTypes: any);
    addEventType(eventType: any): void;
    allowedEvent(event: any): boolean;
    on(event: any, cb: any): {
        deregister: () => void;
        eventID: string;
        isEventListenerReference: () => boolean;
    };
    once(event: any, cb: any): {
        deregister: () => void;
        eventID: string;
        isEventListenerReference: () => boolean;
    };
    trigger(event: any, eventData: any): void;
    exposee(obj: any): void;
    getEventTypes(): any;
}
