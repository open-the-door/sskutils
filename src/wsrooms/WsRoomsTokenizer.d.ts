export default class WsRoomsTokenizer {
    private _opts;
    private _accessKeyMngr;
    constructor(wsRoomsInst: any, opts?: {});
    claimAccessKey(key: any): any;
    authMethod(...args: any[]): any;
}
