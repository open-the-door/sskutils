export default class WsRoomSocketClient {
    protected _active: any;
    protected _authObject: any;
    protected _evtInst: any;
    protected _registrationResult: any;
    protected _remoteEvents: any;
    protected _socketClient: any;
    constructor(host: any, authObject: any);
    _register(): void;
    _setupTriggerEventHandlers(): void;
    _triggerActivation(): void;
    isWsRoomSocketClient(): boolean;
    addHandle(...arg: any[]): any;
    removeHandle(name: any): any;
    clearAllHandles(): void;
    on(event: any, cb: any): any;
    trigger(event: any, eventData: any): any;
    call(...arg: any[]): any;
    ask(lookupObj: any): {
        to: (method: any, ...args: any[]) => any;
    };
}
