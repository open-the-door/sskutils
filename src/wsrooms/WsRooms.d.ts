export default class WsRooms {
    private _wsServer;
    private _events;
    private _rooms;
    private _props;
    constructor(httpServer: any);
    isRooms(): boolean;
    _onConnect(socketArg: any): void;
    _matchRoom(identifierInst: any, socketInst: any): boolean;
    authMethod: any;
    addRoom(roomInst: any): {
        deregister: () => void;
        getRoom: () => any;
        isRoomRef: () => boolean;
        isRoomReference: () => boolean;
        roomID: string;
    };
}
