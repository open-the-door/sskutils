import SocketServer from '../socket/SocketServer';
export declare class WsRoomSocketServer extends SocketServer {
    isWsRoomSocketServer(): boolean;
}
