import { PropWrap } from '../helpers';
export default class WsRoom extends PropWrap {
    private _events;
    private _eventUid;
    private _participants;
    private _participantUid;
    private _roomEvents;
    private roomEvents;
    constructor(proporties?: {});
    roomId: any;
    isRoom(): boolean;
    acceptParticipant(identifierInst: any, wsRoomSocketServerInst: any): {
        isMatchObject: () => boolean;
        participantId: any;
        roomId: any;
    };
    findParticipant(lookupObj: any, excludeParticipantId?: boolean): any[];
    ask(participantId: any, lookupObj: any, method: any, ...args: any[]): any;
    registerEventListener(participantId: any, event: any): any;
    deregisterEventListener(pId: any, eventID: any): boolean;
    triggerEvent(participantId: any, event: any, eventData: any): void;
    removeEventsForParticipant(participantID: any): void;
    removeParticipant(participantID: any): void;
    addParticipant(identifierInst: any, wsRoomSocketServerInst: any): {
        isMatchObject: () => boolean;
        participantId: any;
        roomId: any;
    };
}
