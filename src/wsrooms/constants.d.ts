export declare enum constants {
    askHandle = "ask",
    deregisterEventListenerHandle = "deregisterEventListenerHandle",
    registerEventListenerHandle = "registerEventListenerHandle",
    registrationHandle = "register",
    triggerEventHandler = "triggerEventHandler",
    requestRoomAccessKey = "requestRoomAccessKey",
    requestRoomAccessByKey = "requestRoomAccessByKey",
}
