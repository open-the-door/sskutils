import { PropWrap } from '../helpers';
export default class Identifier extends PropWrap {
    constructor(p?: {});
    isIdentifier(): boolean;
}
