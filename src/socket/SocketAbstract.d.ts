export declare class SocketAbstract {
    private _eventListeners;
    private _messageFactory;
    private _rpcPromiseHandler;
    constructor();
    addHandle(name: any, method: any): any;
    removeHandle(name: any): any;
    clearAllHandles(): void;
    _runMsgFactory(msgString: any): any;
    on(event: any, cb: any): {
        eventId: string;
        deregister: () => void;
    };
    once(event: any, cb: any): {
        eventId: string;
        deregister: () => void;
    };
    _trigger(event: any, eventData: any): void;
    getMessage(messageType: any): any;
    _send(...arg: any[]): void;
    close(...arg: any[]): void;
    send(msgInst: any): void;
    call(procedure: any, ...args: any[]): any;
    getEventTypes(): string[];
    allowedEvent(event: any): boolean;
}
