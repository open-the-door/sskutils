import { SocketAbstract } from './SocketAbstract';
export default class SocketServer extends SocketAbstract {
    protected _wsSocket: any;
    constructor(wsSocket: any);
    _send(msgString: any): void;
    close(): void;
}
