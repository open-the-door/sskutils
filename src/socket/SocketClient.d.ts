import { SocketAbstract } from './SocketAbstract';
export default class SocketClient extends SocketAbstract {
    private _conf;
    private _socket;
    private host;
    private running;
    constructor(host: any);
    keepAlive: any;
    setup(): void;
    onClose(): void;
    isRunning(): any;
    _send(msgString: any): void;
}
